function MoveToTree()
	success, tInspectUp = turtle.inspectUp();
	while (turtle.detectUp() == false and tInspectUp.name ~= "minecraft:dirt" ) do
		turtle.dig();
		turtle.forward();
		success, tInspectUp = turtle.inspectUp();
	end
end

function MoveUp()
	while (turtle.detectUp() == false) do
		turtle.up();
	end
end

function DigTree()
	for i = 1, 5 do
		turtle.digDown();
		turtle.down();
	end
end

sizeX = 4;
sizeY = 4;

MoveUp();
for k=1, sizeX-1 do
	for j=1, sizeY-1 do
		turtle.forward();
		MoveToTree();
		DigTree();
		MoveUp();
	end
	if (k%2 == 0) then
		turtle.turnLeft();
		turtle.dig();
		turtle.forward();
		MoveToTree();
		turtle.turnLeft();
	else
		turtle.turnRight();
		turtle.dig();
		turtle.forward();
		MoveToTree();
		turtle.turnRight();
	end
end